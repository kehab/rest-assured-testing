import org.junit.Test;
import static io.restassured.matcher.ResponseAwareMatcher.*;
import static org.hamcrest.Matchers.*;

import static io.restassured.RestAssured.*;
public class Tests_Get {

    @Test
    public void Test1(){
        given().
            get("https://reqres.in/api/users?page=2").
        then().
            statusCode(200).body("data.id[1]",equalTo(8))
                .body("data.first_name",hasItem("Michael"))
                .log().all();

    }
}
