import static io.restassured.RestAssured.*;
import io.restassured.response.Response;

import static io.restassured.matcher.ResponseAwareMatcher.*;
import static org.hamcrest.Matchers.*;
import org.junit.Assert;
import org.junit.Test;

public class Test01_GET {

    @Test
    public void test_01(){
       Response response = get("https://reqres.in/api/users?page=2");

        System.out.println(response.getBody().asString());
        System.out.println(response.asString());
        System.out.println(response.getStatusCode());
        System.out.println(response.getStatusLine());
        System.out.println(response.getHeader("cotent-type"));
        System.out.println(response.getTime());

        int statuscode= response.getStatusCode();
        Assert.assertEquals(statuscode,200);

    }
    @Test
    public void test_02(){
        Response response = get("https://reqres.in/api/users?page=2");

        int statuscode= response.getStatusCode();
        Assert.assertEquals(statuscode,200);
    }

    @Test
    public void test_03(){
        given().get("https://reqres.in/api/users?page=2").then().statusCode(200).body("data.id[0]",equalTo(7));
    }

}
