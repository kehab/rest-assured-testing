import io.restassured.http.ContentType;
import org.json.simple.JSONObject;
import org.junit.Test;

import static io.restassured.RestAssured.given;

public class Test_PUT {


    @Test
    public void test1(){

        JSONObject request = new JSONObject();
        request.put("name","kareem");
        request.put("job","SW");
        System.out.println(request);

        given().
                headers("Content-Type","application/json").
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
                body(request.toJSONString()).
        when().
                put("https://reqres.in/api/users/2").
        then().
                statusCode(200).log().all();

    }
}
