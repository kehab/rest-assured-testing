import io.restassured.http.ContentType;
import org.apache.tools.tar.TarArchiveSparseEntry;
import org.json.simple.JSONObject;
import org.junit.Test;
import static io.restassured.RestAssured.*;

import java.util.HashMap;
import java.util.Map;

public class Test_POST {

    @Test
    public void test1(){
//   using map and convert it using JSON-Simple lib

//        Map<String , Object> map = new HashMap<String,Object>();
//        map.put("name","kareem");
//        map.put("job","SW");
//        System.out.println(map);
//        JSONObject request = new JSONObject(map);
//        System.out.println(request);

        // using request directly
        JSONObject request = new JSONObject();
        request.put("name","kareem");
        request.put("job","SW");
        System.out.println(request);

        given().
            headers("Content-Type","application/json").
            contentType(ContentType.JSON).
            accept(ContentType.JSON).
            body(request.toJSONString()).
        when().
            post("https://reqres.in/api/users").
        then().
            statusCode(201);

    }
}
